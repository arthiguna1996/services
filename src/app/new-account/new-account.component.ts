import { Component, OnInit,EventEmitter,Output } from '@angular/core';
import {LoggingService} from '../logging.service'; //manual , service injection

@Component({
  selector: 'app-new-account',
  templateUrl: './new-account.component.html',
  styleUrls: ['./new-account.component.css'],
  providers: [LoggingService]         //service injection
})
export class NewAccountComponent  {
@Output () accountadded = new EventEmitter<{name: string, status: string}>();

constructor(private logservice: LoggingService){}    //service injection

oncreateaccount(accountname: string, accstatus:string){
this.accountadded.emit({
  name:accountname,
  status:accstatus
});

//const service = new LoggingService();     - Manual
//service.logstatuschange(accstatus);       - Manual


this.logservice.logstatuschange(accstatus);   //Service Injection

}
}
