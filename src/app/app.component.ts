import { ThisReceiver } from '@angular/compiler';
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Logging Services';

  accounts = [
{
   name: 'Savings Account',
   status: 'active'
},

{
  name: 'Current Account',
  status: 'active'
},
  ]

  onaccountadded(newaccount:{name: string,status: string}){   //to avoid creation of model
    this.accounts.push(newaccount);
  }

  onstatuschange(update: {id: number, newstatus: string}){
    this.accounts[update.id].status=update.newstatus;
  }
}
