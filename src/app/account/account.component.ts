import { Component,EventEmitter,Input,Output } from '@angular/core';
import {LoggingService} from '../logging.service'; //service injection

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css'],
  providers: [LoggingService]         //service injection
})

export class AccountComponent {
  constructor(private logservice: LoggingService){}    //service injection
  @Input() account: {name: string, status: string};
  @Input() id: number;
@Output() statuschanged = new EventEmitter <{id: number,newstatus: string}> ();

Onset(status: string){
    this.statuschanged.emit({id: this.id,newstatus: status});
    this.logservice.logstatuschange(status);  //service injection
    //console.log('Server Status Changed, New Status:' +status);
  }
}
